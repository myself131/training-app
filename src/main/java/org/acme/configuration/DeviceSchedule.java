package org.acme.configuration;

import io.quarkus.scheduler.Scheduled;
import org.acme.controller.DeviceController;
import org.acme.model.Device;
import org.acme.repository.DeviceRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.NotFoundException;

@ApplicationScoped
public class DeviceSchedule {
    String version = "version0";
    @Inject
    private DeviceController deviceController;
    @Inject
    private DeviceRepository deviceRepository;

    @Scheduled(every = "5s")
    void changeVersion() {
        String macAddress = "00:00:00:00:00:00";
        Device device = deviceRepository.changeVersion(macAddress, version).orElseThrow(() -> new NotFoundException("do device with such mac"));
        deviceController.broadCast(device);
        if (version == "version0") {
            version = "version1";
        } else {
            version = "version0";
        }
    }
}
