package org.acme.configuration;

import org.modelmapper.ModelMapper;

import javax.inject.Singleton;

public class DeviceConfiguration {
    @Singleton
    public ModelMapper createModelMapper(){
        return new ModelMapper();
    }
}
