package org.acme.service;

import org.acme.dto.DeviceRequestDto;
import org.acme.exception.DeviceExistException;
import org.acme.model.Device;
import org.acme.repository.DeviceRepository;
import org.modelmapper.ModelMapper;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class DeviceServiceImpl implements DeviceService {
    private DeviceRepository deviceRepository;
    private ModelMapper modelMapper;

    @Inject
    public DeviceServiceImpl(DeviceRepository deviceRepository, ModelMapper modelMapper) {
        this.deviceRepository = deviceRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<Device> getAllDevices() {
        return deviceRepository.listAll();
    }

    @Override
    public Device getDeviceByMacAddress(String macAddress) {
        return deviceRepository.findByMacAddressOptional(macAddress).orElseThrow(
                () -> new NotFoundException("No MAC Address found"));

    }

    @Override
    @Transactional
    public Device createDevice(DeviceRequestDto deviceRequestDto) {
        if (!isMacAddressExist(deviceRequestDto)) {
            Device device = modelMapper.map(deviceRequestDto, Device.class);
            deviceRepository.persist(device);
            return device;
        } else {
            throw new DeviceExistException("Device with the same MAC address existed", Response.Status.BAD_REQUEST);
        }

    }

    @Override
    @Transactional
    public Device updateDevice(DeviceRequestDto deviceRequestDto) {
        Device device = deviceRepository.findByMacAddressOptional(deviceRequestDto.getMacAddress()).orElseThrow(
                () -> new NotFoundException("No MAC Address found"));
        modelMapper.map(deviceRequestDto, device);
        return device;
    }

    @Override
    @Transactional
    public void deleteDevice(List<DeviceRequestDto> deviceRequestDtoList) {
        deviceRequestDtoList.stream().forEach(deviceRequestDto -> {
            Device deviceToDelete = deviceRepository.findByMacAddressOptional(deviceRequestDto.getMacAddress())
                    .orElseThrow(() -> new NotFoundException("No MAC Address found"));
            deviceRepository.delete(deviceToDelete);
        });

    }

    private boolean isMacAddressExist(DeviceRequestDto deviceRequestDto) {
        Optional<Device> optionalDevice = deviceRepository.findByMacAddressOptional(deviceRequestDto.getMacAddress());
        return optionalDevice.isPresent();
    }
}
