package org.acme.service;

import org.acme.dto.DeviceRequestDto;
import org.acme.model.Device;

import java.util.List;

public interface DeviceService {
    List<Device> getAllDevices();

    Device getDeviceByMacAddress(String macAddress);

    Device createDevice(DeviceRequestDto deviceRequestDto);

    Device updateDevice(DeviceRequestDto deviceRequestDto);

    void deleteDevice(List<DeviceRequestDto> deviceRequestDtoList);
}
