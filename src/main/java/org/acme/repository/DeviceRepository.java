package org.acme.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.acme.model.Device;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.NotFoundException;
import java.util.Optional;

@ApplicationScoped
public class DeviceRepository implements PanacheRepository<Device> {
    public Optional<Device> findByMacAddressOptional(String macAddress) {
        return (find("macAddress", macAddress).singleResultOptional());
    }

    public Optional<Device> changeVersion(String macAddress, String version) {
        Device device = findByMacAddressOptional(macAddress).orElseThrow(() -> new NotFoundException("no device with this macAddress"));
        device.setVersion(version);
        persist(device);
        return Optional.ofNullable(device);

    }
}
