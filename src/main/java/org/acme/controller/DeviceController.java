package org.acme.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.vertx.core.VertxException;
import org.acme.dto.DeviceWebsocketResponseDto;
import org.acme.model.Device;
import org.acme.websocket.StartWebSocket;
import org.acme.dto.DeviceRequestDto;
import org.acme.service.DeviceService;
import org.modelmapper.ModelMapper;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.sse.OutboundSseEvent;
import javax.ws.rs.sse.Sse;
import javax.ws.rs.sse.SseBroadcaster;
import javax.ws.rs.sse.SseEventSink;
import java.util.List;


@Path("/devices")
@Singleton
public class DeviceController {
    private DeviceService deviceService;
    private StartWebSocket startWebSocket;
    private ModelMapper modelMapper;
    private SseBroadcaster sseBroadcaster;
    @Context
    private Sse sse;
    private OutboundSseEvent.Builder eventBuilder;

    @Inject
    public DeviceController(DeviceService deviceService, StartWebSocket startWebSocket, ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
        this.deviceService = deviceService;
        this.startWebSocket = startWebSocket;
    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllDevices() {

        return Response.status(Response.Status.OK).entity(deviceService.getAllDevices()).build();
    }

    @GET
    @Path("/device/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDeviceByMacAddress(@QueryParam("mac") String macAddress) {
        return Response.status(Response.Status.OK).entity(deviceService.getDeviceByMacAddress(macAddress)).build();
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON, MediaType.SERVER_SENT_EVENTS})
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createDevice(@Valid DeviceRequestDto deviceRequestDto) {
        Device device = deviceService.createDevice(deviceRequestDto);
        DeviceWebsocketResponseDto deviceWebsocketResponseDto = modelMapper.map(device, DeviceWebsocketResponseDto.class);
        deviceWebsocketResponseDto.setOperation("create");
        startWebSocket.broadcast(deviceWebsocketResponseDto);
        OutboundSseEvent sseEvent = createSseEvent("1", deviceWebsocketResponseDto, "device created");
        try {
            this.sseBroadcaster.broadcast(sseEvent);
        } catch (VertxException e) {
            System.out.println(e.getMessage());
        }

        //this.sseEventSink.send(sseEvent);
        return Response.status(Response.Status.CREATED).entity(device).build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateDevice(@Valid DeviceRequestDto deviceRequestDto) {
        Device device = deviceService.updateDevice(deviceRequestDto);
        DeviceWebsocketResponseDto deviceWebsocketResponseDto = modelMapper.map(device, DeviceWebsocketResponseDto.class);
        deviceWebsocketResponseDto.setOperation("update");
        startWebSocket.broadcast(deviceWebsocketResponseDto);
        return Response.status(Response.Status.ACCEPTED).entity(device).build();
    }

    @DELETE
    @Consumes({MediaType.APPLICATION_JSON})
    public Response deleteDevices(List<@Valid DeviceRequestDto> deviceRequestDtoList) {
        deviceService.deleteDevice(deviceRequestDtoList);
        broadcastListOfDevice(deviceRequestDtoList);
        return Response.status(Response.Status.ACCEPTED).build();
    }

    private void broadcastListOfDevice(List<DeviceRequestDto> deviceList) {
        deviceList.stream().forEach(device -> {
            DeviceWebsocketResponseDto deviceWebsocketResponseDto = modelMapper.map(device, DeviceWebsocketResponseDto.class);
            deviceWebsocketResponseDto.setOperation("delete");
            startWebSocket.broadcast(deviceWebsocketResponseDto);
        });
    }

    @GET
    @Path("/register")
    @Produces(value = MediaType.SERVER_SENT_EVENTS)
    public Response registerServerEvent(@Context SseEventSink sseEventSink) {
        if (this.sseBroadcaster == null) {
            this.sseBroadcaster = this.sse.newBroadcaster();
            this.eventBuilder = this.sse.newEventBuilder();
            this.sseBroadcaster.onError((eventSink, throwable) -> eventSink.close());

        }

        this.sseBroadcaster.register(sseEventSink);
        //this.sseEventSink=sseEventSink;
        return Response.status(Response.Status.OK).build();
    }

    public <T> void broadCast(T someObject) {
        if (this.sse == null) {
            return;
        }
        OutboundSseEvent sseEvent = createSseEvent("1", someObject, "modified");
        try {
            this.sseBroadcaster.broadcast(sseEvent);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


    }

    private <T> OutboundSseEvent createSseEvent(String id, T someObject, String comment) {
        OutboundSseEvent sseEvent = eventBuilder.id(id)
                .mediaType(MediaType.APPLICATION_JSON_TYPE)
                .data(someObject.getClass(), someObject)
                .reconnectDelay(4000)
                .comment(comment)
                .build();
        return sseEvent;
    }


}