package org.acme.exception;

import org.jboss.resteasy.api.validation.ResteasyViolationException;

import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DeviceConstraintViolationExceptionHandler implements ExceptionMapper<ResteasyViolationException> {
    @Override
    public Response toResponse(ResteasyViolationException e) {
        return Response.status(Response.Status.BAD_REQUEST).entity((e.getViolations())).build();
    }
}
