package org.acme.exception;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;

public class DeviceExistException extends ClientErrorException {

    public DeviceExistException(String message, Response.Status status) {
        super(message, status);
    }
}
