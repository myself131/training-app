package org.acme.exception;

import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DeviceExceptionHandler implements ExceptionMapper<DeviceExistException> {

    @Override
    public Response toResponse(DeviceExistException e) {
        return Response.status(e.getResponse().getStatus()).entity(new DeviceExistResponse(e.getMessage())).build();
    }
}
