package org.acme.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DeviceWebsocketResponseDto {
    private String operation;
    private String name;
    private String address;
    private String macAddress;
    private String status;
    private String type;
    private String version;
}
