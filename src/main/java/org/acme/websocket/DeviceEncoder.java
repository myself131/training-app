package org.acme.websocket;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.acme.dto.DeviceWebsocketResponseDto;
import org.acme.model.Device;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

public class DeviceEncoder implements Encoder.Text<DeviceWebsocketResponseDto> {
    private ObjectMapper objectMapper=new ObjectMapper();

    @Override
    public String encode(DeviceWebsocketResponseDto device) throws EncodeException {
        String result;
        try {
            result= objectMapper.writeValueAsString(device);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            result="";
        }
        return result;
    }

    @Override
    public void init(EndpointConfig endpointConfig) {

    }

    @Override
    public void destroy() {

    }
}
