package org.acme.validation;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class TypeValidator implements ConstraintValidator<Type,String> {
    @Override
    public void initialize(Type constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        boolean result=true;
        if (!(s.equals("Up")||s.equals("Down")||s.equals("Warning"))){
            result=false;
        }
        return result;
    }
}
