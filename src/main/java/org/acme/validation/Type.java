package org.acme.validation;



import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
@Constraint(validatedBy = TypeValidator.class)
@Target(value = {ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Type {
    String message() default "Type must be Up, Down or Warning";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
