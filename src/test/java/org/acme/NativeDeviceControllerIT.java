package org.acme;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeDeviceControllerIT extends DeviceControllerTest {

    // Execute the same tests but in native mode.
}